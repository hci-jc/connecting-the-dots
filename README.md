# Connecting The Dots



## Description

This repository contains a set of organized writing [entries](https://hci-jc.gitlab.io/connecting-the-dots/) which may be used for future reference.<br>


## Usage

All materials of this repository are available for adaptation.

```
cd parent_repo
git clone https://gitlab.com/hci-jc/connecting-the-dots.git
```

