---
# Landing Page Content
title: "Welcome"
image: profile.jpg
about:
  template: jolla
  # links:
  #   - icon: twitter
  #     text: Twitter
  #     href: https://twitter.com
  #   - icon: linkedin
  #     text: LinkedIn
  #     href: https://linkedin.com
  #   - icon: github
  #     text: Github
  #     href: https://github.com
  #   - icon: gitlab
  #     text: Gitlab Repo
  #     href: https://gitlab.com/hci-jc

---

Documenting interesting topics\
Condensings learnings into bitesize narratives\
Putting thoughts onto a blank page
